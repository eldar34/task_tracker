<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use app\models\Sprints;
use yii\db\Query;

/**
 * SprintsSearch represents the model behind the search form of `app\models\Sprints`.
 */
class SprintsSearch extends Sprints
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'project_id', 'created_by', 'updated_by'], 'integer'],
            [['title', 'begint_date', 'end_date', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Sprints::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'project_id' => $this->project_id,
            'begint_date' => $this->begint_date,
            'end_date' => $this->end_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['ilike', 'title', $this->title]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchPlan($params)
    {
        $query = new Query();
        $query->select([
            'Пункт' => 'task_id',
            'sprint_id',
            'Задача' => 'tasks.title',
            'План' => 'plan',
            'Факт' => 'fact'
            ])
        ->from('sprints_tasks')
        ->innerJoin('tasks', 'task_id = tasks.id')
        ->innerJoin('sprints', 'sprint_id = sprints.id')
        ->where(['sprint_id'=>$params['sprint_id']]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $prepare_data = [];

        foreach($dataProvider->getModels() as $key => $val){
            $prepare_data[$key] = array_merge($val, $this->unpack_interval($val));    
        }  
        
        $dataProvider2 = new ArrayDataProvider([
            'allModels' =>  $prepare_data,
            'pagination' => [
                'pageSize' => false,
            ],

            'sort' => [
                'attributes' => ['Пункт', 'Задача'],
            ],
        ]);
       
        return $dataProvider2;
    }

    private function unpack_interval($tasks){

        $sprint_tasks = SprintsTasks::find()
        ->select('interval')
        ->where([
                'sprint_id' => $tasks['sprint_id'],
                'task_id' => $tasks['Пункт'],
            ])
        ->one();
        
        return $sprint_tasks->interval;
    }
}
