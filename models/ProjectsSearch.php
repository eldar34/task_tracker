<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Projects;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * ProjectsSearch represents the model behind the search form of `app\models\Projects`.
 */
class ProjectsSearch extends Projects
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by'], 'integer'],
            [['title', 'description', 'managers', 'workers', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Projects::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['ilike', 'title', $this->title])
            ->andFilterWhere(['ilike', 'description', $this->description])
            ->andFilterWhere(['ilike', 'workers', $this->workers])
            ->andFilterWhere(['ilike', 'managers', $this->managers]);

        return $dataProvider;
    }

     /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchBacklog($params)
    {
        
        $query_tasks = new Query();
        $query_tasks->select([
            'id',
            'title',
            ])
        ->from('tasks')
        ->where(['project_id'=>$params['id']]);

        

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query_tasks,
        ]);

        
        $backlog = $this->full_backlog($dataProvider->getModels(), $params['id']);

        return $backlog;

    }

    private function full_backlog($tasks_array, $project_id){
        $result = [];
        foreach($tasks_array as $task){
            
            $query_sprints = new Query();
            $query_sprints->select(['sprint_id', 'plan', 'fact'])
            ->from('sprints_tasks')
            ->where(['task_id' => $task['id'], 'project_id'=>$project_id]);
            $change_indexes = ArrayHelper::index($query_sprints->all(), 'sprint_id');
            $task['sprints'] = $change_indexes;
           
            array_push($result, $task);
        }
        return $result;

    }
}
