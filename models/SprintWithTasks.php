<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

class SprintWithTasks extends Sprints
{
    /**
     * @var array IDs of the tasks
     */
    public $tasks_ids = [];

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            // each category_id must exist in category table (*1)
            ['tasks_ids', 'each', 'rule' => [
                'exist', 'targetClass' => Tasks::className(), 'targetAttribute' => 'id'
            ]],
        ]);
    }


 

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'tasks_ids' => 'Задачи',
        ]);
    }

    /**
     * load the post's tasks (*2)
     */
    public function loadTasks()
    {
        $this->tasks_ids = [];
        if (!empty($this->id)) {
            $rows = SprintsTasks::find()
                ->select(['task_id'])
                ->where(['sprint_id' => $this->id])
                ->asArray()
                ->all();
            foreach ($rows as $row) {
                $this->tasks_ids[] = $row['task_id'];
            }
        }
    }

    /**
     * save the sprint's tasks (*3)
     */
    public function saveTasks()
    {
        /* clear the tasks of the sprint before saving */
        SprintsTasks::deleteAll(['sprint_id' => $this->id]);
        if (is_array($this->tasks_ids)) {
            $dates_interval = $this->date_with_status($this->begint_date, $this->end_date);
            $prepare_interval = \yii\helpers\Json::encode($dates_interval);
            foreach ($this->tasks_ids as $task_id) {
                $sp = new SprintsTasks();
                $sp->sprint_id = $this->id;
                $sp->task_id = $task_id;
                $sp->project_id = $this->project_id;
                $sp->interval = $prepare_interval;
                $sp->save();
            }
        }
        /* Be careful, $this->tasks_ids can be empty */
    }

     /**
     * @return array the dates interval with status.
     */

    private function date_with_status($begin, $end)
    {
        $begin = new \DateTime($begin);
        $end = new \DateTime($end);
        $end = $end->modify('+1 day');

        $interval = new \DateInterval('P1D');
        $daterange = new \DatePeriod($begin, $interval, $end);

        $result = [];

        foreach ($daterange as $date) {
            
            $result[$date->format("Y-m-d")] = '1%';      
            
        }

        return $result;
    }
}
