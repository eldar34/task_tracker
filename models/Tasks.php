<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tasks".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $description
 * @property int|null $raiting
 * @property int|null $labor_costs
 * @property int|null $appointed
 * @property string|null $comment
 * @property int|null $status
 * @property int|null $project_id
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 *
 * @property Projects $project
 */
class Tasks extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tasks';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['raiting', 'labor_costs', 'appointed', 'status', 'project_id', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['raiting', 'labor_costs', 'appointed', 'status', 'project_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'comment'], 'string', 'max' => 255],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Projects::className(), 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    public function behaviors()
    {
        return [

            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],

            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function () {
                    return date('Y-m-d H:i:s');
                },
            ]
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'description' => 'Описание задачи',
            'raiting' => 'Коллективная оценка',
            'labor_costs' => 'Фактические трудозатраты',
            'appointed' => 'Назначена',
            'comment' => 'Комментарий',
            'status' => 'Статус',
            'project_id' => 'Проект',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'created_by' => 'Создал',
            'updated_by' => 'Обновил',
        ];
    }

    /**
     * Gets query for [[Project]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['id' => 'project_id']);
    }

    /**
     * Get all the available tasks (*4)
     * @return array available tasks
     */
    public static function getAvailableTasks()
    {
        $tasks = self::find()->orderBy('title')->asArray()->all();
        $items = ArrayHelper::map($tasks, 'id', 'title');
        return $items;
    }
}
