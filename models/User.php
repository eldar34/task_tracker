<?php

namespace app\models;
use mdm\admin\models\User as UserModel;

class User extends UserModel
{

    public function rules()
    {
        $rules = parent::rules();
        $rules['full_name']   = ['full_name', 'string', 'min' => 2, 'max' => 255];
        

        return $rules;
    }
    
}
