<?php

namespace app\models;

use mdm\admin\models\form\Signup as MdmSignupForm;
use yii\helpers\ArrayHelper;
use mdm\admin\components\UserStatus;



class SignupForm extends MdmSignupForm
{
    public $full_name;
    public $shelf_life;
    public $user_role;
    public $first_entry;
    public $unit_id;
    public $user_post;
    public $user_bid;

    public function rules()
    {
        $rules = parent::rules();
        $rules['user_name'] = ['username', 'match', 'pattern' => '/^[a-z]\w*$/i'];
        $rules['user_name_length'] = ['username', 'string', 'min' => 4, 'max' => 12];
        $rules['full_name'] = ['full_name', 'required'];
       

        return $rules;
    }

    public function signup()
    {
        if ($this->validate()) {
            $class = \Yii::$app->getUser()->identityClass ? : 'mdm\admin\models\User';
            $user = new $class();
            $user->username = mb_strtolower($this->username);
            $user->full_name = $this->full_name;
            $user->email = $this->email;
            $user->status = ArrayHelper::getValue(\Yii::$app->params, 'user.defaultStatus', UserStatus::ACTIVE);
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Логин',
            // 'content' => 'Content',
            'full_name' => 'ФИО',
            
        ];
    }

    
    
}