<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "sprints".
 *
 * @property int $id
 * @property string|null $title
 * @property int|null $project_id
 * @property string|null $begint_date
 * @property string|null $end_date
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $plan
 * @property int|null $fact
 * @property int|null $interval
 *
 * @property Projects $project
 * @property SprintsTasks[] $sprintsTasks
 */
class Sprints extends \yii\db\ActiveRecord
{
    public $plan;
    public $fact;
    public $interval;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sprints';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['project_id', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['project_id', 'created_by', 'updated_by', 'plan', 'fact'], 'integer'],
            [['begint_date', 'end_date', 'created_at', 'updated_at', 'interval'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Projects::className(), 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    public function behaviors()
    {
        return [

            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],

            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function () {
                    return date('Y-m-d H:i:s');
                },
            ]
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Наименование',
            'project_id' => 'Проект',
            'begint_date' => 'Дата начала',
            'end_date' => 'Дата окончания',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'created_by' => 'Создал',
            'updated_by' => 'Обновил',
        ];
    }

    /**
     * Gets query for [[Project]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['id' => 'project_id']);
    }

    /**
     * Gets query for [[SprintsTasks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSprintsTasks()
    {
        return $this->hasMany(SprintsTasks::className(), ['sprint_id' => 'id']);
    }
}
