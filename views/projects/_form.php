<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Projects */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="projects-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true, 'autocomplete' => "off"]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?php // $form->field($model, 'participants')->textInput() ?>
    <?php
        $users = ArrayHelper::map(\app\models\User::find()->select(['id', 'full_name'])->orderBy('full_name')->asArray()->all(), 'id', 'full_name');
        // dx($users);
        echo $form->field($model, 'managers')->widget(Select2::classname(), [
            'data' => $users,
            'options' => ['placeholder' => 'Выберите менеджеров...'],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple' => true
            ],
            'pluginEvents' => [
                "select2:opening" => "function() { 
                    //Get value from workers select
                    let selected_options = $('#projects-workers').val(); 

                    //Get all options from managers select
                    let managers_options = $('#projects-managers').find('option');

                    //For each managers_option compare with selected_options
                    managers_options.each(function( index ) {
                        if(selected_options.indexOf($( this ).val()) != -1)
                        {  
                            $( this ).prop('disabled', true);
                        }else{
                            $( this ).prop('disabled', false);

                        }
                      
                      });                   
                }",                
            ]
        ]);

        echo $form->field($model, 'workers')->widget(Select2::classname(), [
            'data' => $users,
            'options' => ['placeholder' => 'Выберите исполнителей...'],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple' => true
            ],
            'pluginEvents' => [
                "select2:opening" => "function() { 
                    //Get value from managers select
                    let selected_options = $('#projects-managers').val(); 

                    //Get all options from workers select
                    let workers_options = $('#projects-workers').find('option');

                    //For each workers_option compare with selected_options
                    workers_options.each(function( index ) {
                        if(selected_options.indexOf($( this ).val()) != -1)
                        {  
                            $( this ).prop('disabled', true);
                        }else{
                            $( this ).prop('disabled', false);

                        }
                      
                      });                   
                }",                
            ]
        ]);
    ?>
   

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
