<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Projects */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="projects-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'description:ntext',
          
            [
                'attribute'=>'managers',
                'value'=>function($data){
                    $result = [];
                    foreach($data->managers as $manager_id){
                        $full_name = app\models\User::findOne($manager_id)->full_name;
                        $result[] = $full_name;
                    }
                    return implode(", ", $result);
                }
            ],
            [
                'attribute'=>'workers',
                'value'=>function($data){
                    $result = [];
                    foreach($data->workers as $manager_id){
                        $full_name = app\models\User::findOne($manager_id)->full_name;
                        $result[] = $full_name;
                    }
                    return implode(", ", $result);
                }
            ],

            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
