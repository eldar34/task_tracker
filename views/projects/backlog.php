<?php

use Codeception\PHPUnit\ResultPrinter\HTML as ResultPrinterHTML;
use yii\bootstrap4\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Бэклог';
$this->params['breadcrumbs'][] = $this->title;

       
// dx(empty($dataProvider)); 

?>
<div class="projects-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <hr>

    <p>
        <?= Html::a('Создать задачу', ['tasks/create', 'project_id' => $id], ['class' => 'btn btn-success']) ?>
        
        <?php if(empty($dataProvider)): ?>
            <?= Html::button('Создать спринт',  ['class' => 'btn btn-primary', 'disabled'=>true]) ?>

        <?php else: ?>
            <?= Html::a('Создать спринт', ['sprints/create', 'project_id' => $id], ['class' => 'btn btn-primary']) ?>
        <?php endif ?>
        <span class="float-right"><button class="btn btn-secondary" id="export-table">Экспорт ExCel</button></span>
    </p>


    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?php
    // GridView::widget([
    //     'dataProvider' => $dataProvider,
    //     // 'filterModel' => $searchModel,
    //     'beforeHeader'=>[
    //         [
    //             'columns'=>[
    //                 // ['content'=>'Header Before 1', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
    //                 ['content'=>'', 'options'=>['colspan'=>2, 'class'=>'text-center warning']], 
    //                 ['content'=>'Спринт 1', 'options'=>['colspan'=>2, 'class'=>'text-center warning']], 
    //             ],
    //             'options'=>['class'=>'skip-export'] // remove this row from export
    //         ]
    //     ],

    //     'columns' => [
    //         //['class' => 'yii\grid\SerialColumn'],


    //         [
    //             'attribute'=>'task_id',
    //             'label'=> 'Пункт'
    //         ],

    //         [
    //             'attribute'=>'task_title',
    //             'label'=> 'Задача'
    //         ],
    //         'plans',
    //         'facts'


    //         // ['class' => 'yii\grid\ActionColumn'],
    //     ],
    // ]); 

    ?>

    <?php
        $sprints_ids = ArrayHelper::getColumn($sprints, 'id');
        // dx(empty($dataProvider));

    ?>

    <table class="table table-bordered" id="my-grid-id">
        <thead>
            <tr>
                <td colspan="2"></td>
                <?php foreach ($sprints as $sprint) : ?>
                    <th colspan="2"><?= Html::a($sprint['title'], ['sprints/sprint-plan', 'sprint_id' => $sprint['id']]) ?></th>
                <?php endforeach ?>
            </tr>
            <tr>
                <th scope="col-4">Пункт</th>
                <th scope="col-8">Задача</th>
                <?php foreach ($sprints as $sprint) : ?>
                    <th scope="row">План</th>
                    <th scope="row">Факт</th>
                <?php endforeach ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dataProvider as $task) : ?>
                <tr>
                    <th scope="row"><?= $task['id'] ?></th>
                    <td><?= $task['title'] ?></td>
                    <?php foreach ($sprints_ids as $id) : ?>
                        <?php if(array_key_exists($id, $task['sprints'])): ?>
                            <td scope="row"><?= $task['sprints'][$id]['plan'] ?></td>
                            <td scope="row"><?= $task['sprints'][$id]['fact'] ?></td>
                        <?php else: ?>
                            <td scope="row"></td>
                            <td scope="row"></td>
                        <?php endif ?>
                        
                    <?php endforeach ?>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>


</div>

<?php
$this->registerJsFile("@web/template/js/tableToExcel.js",[
    'depends' => [
        \yii\web\JqueryAsset::className()
    ]
]);
?>

<?php
$js = <<< JS


    $('#export-table').click(function () {
    
        TableToExcel.convert(document.getElementById("my-grid-id"));
        

    });

JS;

$this->registerJs($js, $position = yii\web\View::POS_READY, $key = null);
?>



