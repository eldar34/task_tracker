<?php

use Codeception\PHPUnit\ResultPrinter\HTML as ResultPrinterHTML;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Проекты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projects-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать проект', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute'=>'title',
                'format' => 'raw',
                'value'=>function($data){
                    return HTML::a($data->title, ['backlog', 'id'=>$data->id]);
                }
            ],
            'description:ntext',
            // 'participants',
            [
                'attribute'=>'managers',
                'filter'=>'',
                'value'=>function($data){
                    $result = [];
                    foreach($data->managers as $manager_id){
                        $full_name = app\models\User::findOne($manager_id)->full_name;
                        $result[] = $full_name;
                    }
                    return implode(", ", $result);
                }
            ],
            [
                'attribute'=>'workers',
                'filter'=>'',
                'value'=>function($data){
                    $result = [];
                    foreach($data->workers as $manager_id){
                        $full_name = app\models\User::findOne($manager_id)->full_name;
                        $result[] = $full_name;
                    }
                    return implode(", ", $result);
                }
            ],
            
            'created_at',
            //'updated_at',
            //'created_by',
            //'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
