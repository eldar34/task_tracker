<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\UserGrid */

$this->title = "Просмотр пользователя: " . $model->full_name;

?>
<div class="user-grid-view">

<h1 class="text-center mt-3"><?= Html::encode($this->title) ?></h1>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-7 mt-3">
                <p>
                    <?= Html::a('Обновить', ['update-user', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Удалить', ['delete-user', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </p>

                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'username',
                            'email:email',
                            'created_at',
                            'updated_at',
                            'full_name',
                            
                        ],
                    ]) ?>
            </div>
        </div>
    </div>

    

    

</div>
