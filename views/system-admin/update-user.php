<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserGrid */

$this->title = 'Обновление пользователя: ' . $model->full_name;

?>
<div class="user-grid-update">
    <h1 class="text-center mt-3"><?= Html::encode($this->title) ?></h1>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-5 mt-3">
                <?= $this->render('_form-user', [
                    'model' => $model,
                ]) ?>
            </div>
        </div>
    </div>





</div>