<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\bootstrap\Alert;

use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\TblProjects */

$this->title = 'Пользователи';
// $this->params['breadcrumbs'][] = ['label' => 'Tbl Projects', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;

?>
<div class="system-admin-view">

    <div class="row">

        <div class="col pt-4">
            <div class="container-fluid">
                <div class="row">
                    <div class="col mt-4">
                    <p>
                        <?= Html::a('Добавить пользователя', ['/user/signup'], ['class' => 'btn btn-primary']) ?>
                    </p>

                            <?= GridView::widget([
                                'dataProvider' => $dataProvider,
                                'layout'=>"<span class='float-right'>$this->title</span>{summary}\n{items}\n{pager}",
                                'filterModel' => $searchModel,
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],

                                    'id',
                                    'username',
                                    // 'auth_key',
                                    // 'password_hash',
                                    // 'password_reset_token',
                                    //'email:email',
                                    //'status',
                                    //'created_at',
                                    // 'updated_at',
                                    'full_name',
                                   

                                    [
                                        'class' => 'yii\grid\ActionColumn',

                                        'urlCreator' => function ($action, $model, $key, $index) {
                                            if ($action === 'view') {
                                                $url ='view-user?id='.$model->id;
                                                return $url;
                                            }
                                
                                            if ($action === 'update') {
                                                $url ='update-user?id='.$model->id;
                                                return $url;
                                            }
                                            if ($action === 'delete') {
                                                $url ='delete-user?id='.$model->id;
                                                return $url;
                                            }
                                
                                        }
                                    ],
                                ],
                            ]); ?>

                    </div>
                    
                </div>
                
            </div>
        </div>

    </div>

    



</div>