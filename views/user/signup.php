<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \mdm\admin\models\form\Signup */

$this->title =  'Создание пользователя';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">

    <h1 class="text-center mt-3"><?= Html::encode($this->title) ?></h1>

    <?php // Html::errorSummary($model) ?>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-5 mt-3">
                <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
                <?= $form->field($model, 'username') ?>
                <?= $form->field($model, 'email') ?>
                <?= $form->field($model, 'full_name')->label('ФИО') ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <?= $form->field($model, 'retypePassword')->passwordInput() ?>
                <div class="form-group">
                    <?= Html::submitButton('Создать', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>



</div>