<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Tasks */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tasks-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'raiting')->textInput() ?>

    <?= $form->field($model, 'labor_costs')->textInput() ?>

    <?php
        $workers = app\models\Projects::findOne($project_id)->workers;
    ?>

    <?= $form->field($model, 'appointed', ['enableClientValidation' => false])->dropdownList(
        ArrayHelper::map(\app\models\User::find()->select(['id', 'full_name'])->where(['in', 'id', $workers])->asArray()->all(), 'id', 'full_name'),
        [
            'prompt' => 'Выберите исполнителя',
        ]
    ); ?>

    <?= $form->field($model, 'comment')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status', ['enableClientValidation' => false])->dropdownList(
        ArrayHelper::map(\app\models\Status::find()->select(['id', 'title'])->asArray()->all(), 'id', 'title'),
        [
            'prompt' => 'Выберите статус',
        ]
    ); ?>



    <?= $form->field($model, 'project_id', ['enableClientValidation' => false])->dropdownList(
        ArrayHelper::map(\app\models\Projects::find()->select(['id', 'title'])->asArray()->all(), 'id', 'title'),
        [
            'options' => [$project_id => ['Selected' => 'selected']],
            'prompt' => 'Выберите проект',
            'disabled' => 'disabled'
        ]
    ); ?>


    <div class="form-group">
        <?= Html::submitButton('Создать', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>