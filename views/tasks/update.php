<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tasks */

$this->title = 'Обновить задачу: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Tasks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить задачу';
?>
<div class="tasks-update">

    <h1 class="text-center mt-3"><?= Html::encode($this->title) ?></h1>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-5 mt-3">
                <?= $this->render('_form', [
                    'model' => $model,
                    'project_id' => $project_id
                ]) ?>
            </div>
        </div>
    </div>

</div>