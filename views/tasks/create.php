<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tasks */

$this->title = 'Создать задачу';
$this->params['breadcrumbs'][] = ['label' => 'Бэклог', 'url' => ['projects/backlog', 'id' => $project_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tasks-create">

    <h1 class="text-center mt-3"><?= Html::encode($this->title) ?></h1>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-5 mt-3">
                <?= $this->render('_form', [
                    'model' => $model,
                    'project_id' => $project_id
                ]) ?>
            </div>
        </div>
    </div>

</div>