<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap4\Alert;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\bootstrap4\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body>
    <?php $this->beginBody() ?>

    <div class="wrap">
        <?php
        NavBar::begin([
            'brandOptions' => [
                'class' => ['pl-0']
            ],
            'togglerOptions' => [
                'class' => ['order-1']
            ],
            'brandLabel' => Yii::$app->name,
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => ['navbar-dark', 'bg-dark', 'navbar-expand-md'],
            ],
            'collapseOptions' => ['class' => 'col justify-content-end pr-0'],
            // 'innerContainerOptions' => ['class' => ['container-fluid']], 
            'renderInnerContainer' => false,
        ]);
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav mr-2'],
            'items' => [
                [
                    'label' => 'Админ',

                    'visible' => in_array(Yii::$app->user->identity->username, ['admin']),
                    'items' => [
                        ['label' => 'Пользователи', 'url' => ['/system-admin/index']],
                        ['label' => 'Панель', 'url' => ['/admin']]
                    ]
                ],
                // ['label' => 'Главная', 'url' => ['/site/index']],
                ['label' => 'Проекты', 'url' => ['/projects/index']],

                [
                    'label' => 'Войти',
                    'url' => ['/site/login'],
                    'visible' => Yii::$app->user->isGuest,
                ],

                [
                    'label' => 'Выйти (' . Yii::$app->user->identity->username . ')',
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post'],
                    'visible' => !Yii::$app->user->isGuest,
                ]


            ],
        ]);
        NavBar::end();
        ?>

        <div class="container-fluid pt-4">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>

            <?php
                if (Yii::$app->session->hasFlash('access-denied')) {
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-danger',
                        ],
                        'closeButton' => ['class' => 'close text-white'],
                        'body' => Yii::$app->session->getFlash('access-denied'),
                    ]);
                }
            ?>

            
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container-fluid">

            <div class="row justify-content-between">
                <div class="col">
                    <p class="pull-left">&copy; Бэклог <?= date('Y') ?></p>
                </div>

                <div class="col col-auto mr-5">
                    <p class="pull-right"><?= Yii::powered() ?></p>
                </div>
            </div>

        </div>
    </footer>

    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>