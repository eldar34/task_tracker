<?php

use yii\bootstrap4\Html;
use yii\grid\GridView;
use yii\bootstrap4\Modal;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SprintsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$items = $dataProvider->allModels;
$current_sprint = \app\models\Sprints::findOne(['id'=> $items[0]['sprint_id']]);

$this->title = $current_sprint->title;

$this->params['breadcrumbs'][] = ['label' => 'Бэклог', 'url' => ['projects/backlog', 'id' => $current_sprint->project_id]];
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = $table_columns;

// dx($dataProvider->allModels);
?>
<div class="sprints-index">





    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <div class="row">
        <div class="col-10">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="col-2">
            <?php
                echo ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'showConfirmAlert' => false,
                    'fontAwesome' => true,
                    'container' => ['class' => 'btn-group mb-5'],
                    'exportConfig' => [
                        ExportMenu::FORMAT_TEXT => false,
                        ExportMenu::FORMAT_PDF => false,
                        ExportMenu::FORMAT_HTML => false,
                        ExportMenu::FORMAT_CSV => false,
                        ExportMenu::FORMAT_EXCEL => [
                            'icon' => 'fas fa-file-excel'
                        ]
                    ],
                ]);
            ?>
        </div>
    </div>


    <?php



    $searchModel2 = [];
    $searchColumns = [];
    $searchAttributes = $table_columns;
    

    foreach ($searchAttributes as $searchAttribute) {

        $filterName = 'filter' . $searchAttribute;
        $filterValue = Yii::$app->request->getQueryParam($filterName, '');
        $searchModel2[$searchAttribute] = $filterValue;

        $searchColumns[] = [
            'attribute' => $searchAttribute,
            'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
            'value' => $searchAttribute,
        ];

        $items = array_filter($items, function ($item) use (&$filterValue, &$searchAttribute) {
            return strlen($filterValue) > 0 ? stripos('/^' . strtolower($item[$searchAttribute]) . '/', strtolower($filterValue)) : true;
        });
    }

    ?>

    <?= GridView::widget([

        // 'layout' => "<span class='float-right'>$this->title</span>{summary}\n{items}\n{pager}",
        // 'filterModel' => $searchModel,

        'tableOptions' => [
            'class' => 'table table-striped table-bordered',
            'id' => 'table_sprint_plan'
        ],

        'dataProvider' => new \yii\data\ArrayDataProvider([
            'allModels' => $items,
            'sort' => [
                'attributes' => $searchAttributes,
            ],
        ]),
        'filterModel' => $searchModel2,
        'columns' => $searchColumns

    ]); ?>

    <!-- Модальное окно rename FILE -->
    <?php
    Modal::begin([
        'title' => 'Обновить',
        'titleOptions' => ['class' => 'ml-auto'],
        'closeButton' => ['class' => 'close text-white'],
        'size' => 'modal-lg modal-dialog-centered',
        'options' => ['id' => 'modal-update-plan'],
    ]);
    ?>


    <?= Html::beginForm(['/sprints/update-plan', 'sprint_id' => $items[0]['sprint_id']], 'post', ['data-pjax' => '', 'id' => 'form-update-plan']); ?>
    <?= Html::hiddenInput('current_task') ?>
    <div class="form-group row">
        <label class="col-sm-4">Обновить План</label>
        <div class="col-sm-8">
            <?= Html::input('text', 'new_plan', null, ['class' => 'form-control', 'autocomplete' => "off"]) ?>
            <div class="invalid-feedback">
                Использованы запрещенные символы [. <'>`"].
            </div>
        </div>
    </div>

    <div class="form-group row">

        <label class="col-sm-4">Обновить Факт</label>
        <div class="col-sm-8">
            <?= Html::input('text', 'new_fact', null, ['class' => 'form-control', 'autocomplete' => "off"]) ?>
            <div class="invalid-feedback">
                Использованы запрещенные символы [. <'>`"].
            </div>
        </div>

    </div>

    <div class="row justify-content-center">
        <?= Html::button('Сохранить', ['class' => 'btn btn-success', 'onclick' => 'updatePlan()']) ?>
        <?= Html::button('Назад', ['class' => 'ml-3 btn btn-secondary', 'data-dismiss' => 'modal']) ?>
    </div>

    <?= Html::endForm() ?>

    <?php Modal::end(); ?>

    <!-- Модальное окно загрузки файла -->


</div>

<script>
    function updatePlan() {
        $("#form-update-plan").submit();

    }
</script>

<?php
$js = <<< JS


    $( "#table_sprint_plan tr:not(.filters)" ).dblclick(function() {
        let task_id = $(this).find('td:first').text();
        let sprint_id = $("input[name='current_task']").val(task_id);
        // console.log(sprint_id);
        $('#modal-update-plan').modal('show');
    });

    

JS;

$this->registerJs($js, $position = yii\web\View::POS_READY, $key = null);
?>