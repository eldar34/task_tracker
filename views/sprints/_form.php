<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Sprints */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sprints-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'project_id', ['enableClientValidation' => false])->dropdownList(
        ArrayHelper::map(\app\models\Projects::find()->select(['id', 'title'])->asArray()->all(), 'id', 'title'),
        [
            'options' => [$project_id => ['Selected' => 'selected']],
            'prompt' => 'Выберите проект',
            'disabled' => 'disabled'
        ]
    ); ?>

    <?= $form->field($model, 'begint_date')->widget(DatePicker::classname(), [
        'removeButton' => false,
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'Дата начала спринта', 'autocomplete' => "off"],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]) ?>

    <?= $form->field($model, 'end_date')->widget(DatePicker::classname(), [
        'removeButton' => false,
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'Дата окончания спринта', 'autocomplete' => "off"],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]) ?>

    <?= $form->field($model, 'tasks_ids')
        ->listBox($tasks, ['multiple' => true])
        /* or, you may use a checkbox list instead */
        /* ->checkboxList($categories) */
        ->hint('Добавить задачи в спринт');
    ?>



    <div class="form-group">
        <?= Html::submitButton('Создать', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>