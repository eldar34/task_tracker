<?php

namespace app\controllers;

use Yii;
use app\models\Sprints;
use app\models\SprintWithTasks;
use app\models\SprintsTasks;
use app\models\Tasks;
use app\models\SprintsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SprintsController implements the CRUD actions for Sprints model.
 */
class SprintsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sprints models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SprintsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Sprints models.
     * @return mixed
     */
    public function actionSprintPlan($sprint_id)
    {
        $searchModel = new SprintsSearch();
        $dataProvider = $searchModel->searchPlan(Yii::$app->request->queryParams);
        $for_columns = $dataProvider->allModels;
       
        
        $table_columns = array_keys($for_columns[0]);
        // delete sprint_id column
        unset($table_columns[1]);
        // dx($dataProvider);

        return $this->render('sprint_plan', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'table_columns' => $table_columns
        ]);
    }

    public function actionUpdatePlan($sprint_id)
    {
        // Get parametrs
        $request = Yii::$app->request;
        $new_plan = $request->post('new_plan');
        $new_fact = $request->post('new_fact');
        $current_task = $request->post('current_task');

        // Get model
        $sprints_docs = SprintsTasks::findOne(['task_id' => $current_task, 'sprint_id' => $sprint_id]);
        $sprints_docs->plan = $new_plan;
        $sprints_docs->fact = $new_fact;
        $sprints_docs->updateAttributes(['plan', 'fact']);

        // Return Grind-view
        $searchModel = new SprintsSearch();
        $dataProvider = $searchModel->searchPlan(Yii::$app->request->queryParams);
        $for_columns = $dataProvider->allModels;
       
        
        $table_columns = array_keys($for_columns[0]);
        // delete sprint_id column
        unset($table_columns[1]);
        // dx($dataProvider);

        return $this->render('sprint_plan', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'table_columns' => $table_columns
        ]);

        
        

    }

    /**
     * Displays a single Sprints model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Sprints model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($project_id)
    {
        $model = new SprintWithTasks();
        $model->project_id = $project_id;

        if($model->load(Yii::$app->request->post())){
            if ($model->save()) {
                $model->saveTasks();
                return $this->redirect(['/projects/backlog', 'id' => $project_id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'project_id' => $project_id,
            'tasks' => Tasks::getAvailableTasks(),
        ]);
    }

    /**
     * Updates an existing Sprints model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = SprintWithTasks::findOne($id);
        $model->loadTasks();

        if($model->load(Yii::$app->request->post())){
            if ($model->save()) {
                $model->saveTasks();
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'project_id' => $model->project_id,
            'tasks' => Tasks::getAvailableTasks(),
        ]);
    }

    /**
     * Deletes an existing Sprints model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Sprints model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sprints the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sprints::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
