<?php

namespace app\controllers;

use app\models\SignupForm;
use app\models\ChangePassword;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;


class UserController extends \yii\web\Controller
{
    /**
     * Signup new user
     * @return string
     */

    

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->getRequest()->post())) {
            // Data for flash message
            $new_user['login'] = mb_strtolower($model->username);
            $new_user['password'] = $model->password;
            

            if ($user = $model->signup()) {
                
                Yii::$app->session->setFlash('new-user-success', $new_user);
                
                return $this->refresh();
            }
        }

        return $this->render('signup', [
                'model' => $model,
        ]);
    }
    /**
     * Change password
     * @return string
     */
    public function actionChangePassword()
    {
        $model = new ChangePassword();
        
        if ($model->load(Yii::$app->getRequest()->post()) && $model->change()) {
            return $this->goHome();
        }

        $this->layout = 'main';

        return $this->render('change-password', [
                'model' => $model,
        ]);
    }

}
