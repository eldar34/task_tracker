<?php

namespace app\controllers;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\ModelhistorySearch;
use mdm\admin\models\searchs\User as UserSearch;
use yii\web\NotFoundHttpException;
use Yii;


class SystemAdminController extends \yii\web\Controller
{

    

    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
        
    }

    public function actionViewUser($id)
    {
        return $this->render('view-user', [
            'model' => $this->findModelUser($id),
        ]);
    }

    public function actionUpdateUser($id)
    {
        $model = $this->findModelUser($id);

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            // Change role
            // $auth = Yii::$app->authManager;
            // $current_role = $auth->getRole($model->user_role);
            // $auth->revokeAll($model->id);
            // $auth->assign($current_role, $model->id);

            return $this->redirect(['view-user', 'id' => $model->id]);
        }

        return $this->render('update-user', [
            'model' => $model,
        ]);
    }

    public function actionDeleteUser($id)
    {
        // Revoke auth_assignment
        $auth = Yii::$app->authManager;        
        $auth->revokeAll($id);
        
        $this->findModelUser($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModelUser($id)
    {
        if (($model = \app\models\User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    

}
