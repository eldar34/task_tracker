<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sprints_tasks}}`.
 */
class m201113_114636_create_sprints_tasks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sprints_tasks}}', [
            'id' => $this->primaryKey(),
            'sprint_id' => $this->integer(),
            'task_id' => $this->integer(),
            'project_id' => $this->integer(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);

        // creates index for column `sprint_id`
        $this->createIndex(
            'idx-sprints_tasks-sprint_id',
            'sprints_tasks',
            'sprint_id'
        );

        // add foreign key for table `sprints_tasks`
        $this->addForeignKey(
            'fk-sprints_tasks-sprint_id',
            'sprints_tasks',
            'sprint_id',
            'sprints',
            'id',
            'CASCADE'
        );

        // creates index for column `task_id`
        $this->createIndex(
            'idx-sprints_tasks-task_id',
            'sprints_tasks',
            'task_id'
        );

        // add foreign key for table `sprints_tasks`
        $this->addForeignKey(
            'fk-sprints_tasks-task_id',
            'sprints_tasks',
            'task_id',
            'tasks',
            'id',
            'CASCADE'
        );

        // creates index for column `project_id`
        $this->createIndex(
            'idx-sprints_tasks-project_id',
            'sprints_tasks',
            'project_id'
        );

        // add foreign key for table `sprints_tasks`
        $this->addForeignKey(
            'fk-sprints_tasks-project_id',
            'sprints_tasks',
            'project_id',
            'projects',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sprints_tasks}}');
    }
}
