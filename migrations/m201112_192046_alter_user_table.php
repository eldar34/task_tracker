<?php

use yii\db\Migration;

/**
 * Class m201112_192046_alter_user_table
 */
class m201112_192046_alter_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'full_name', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'full_name');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201112_192046_alter_user_table cannot be reverted.\n";

        return false;
    }
    */
}
