<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tasks}}`.
 */
class m201113_063744_create_tasks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tasks}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'description'=> $this->text(),
            'raiting' => $this->integer(),
            'labor_costs' => $this->integer(),
            'appointed' => $this->integer(),
            'comment' => $this->string(),
            'status' => $this->integer(),
            'project_id' => $this->integer(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),

        ]);

        // creates index for column `project_id`
        $this->createIndex(
            'idx-tasks-project_id',
            'tasks',
            'project_id'
        );

        // add foreign key for table `tasks.project_id`
        $this->addForeignKey(
            'fk-tasks-project_id',
            'tasks',
            'project_id',
            'projects',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%tasks}}');
    }
}
