<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sprints}}`.
 */
class m201113_112722_create_sprints_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sprints}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'project_id' => $this->integer(),
            'begint_date' => $this->date(),
            'end_date' => $this->date(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);

        // creates index for column `project_id`
        $this->createIndex(
            'idx-sprints-project_id',
            'sprints',
            'project_id'
        );

        // add foreign key for table `sprints`
        $this->addForeignKey(
            'fk-sprints-project_id',
            'sprints',
            'project_id',
            'projects',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sprints}}');
    }
}
