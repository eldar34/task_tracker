<?php

use yii\db\Migration;

/**
 * Class m201113_191946_alter_projects_table
 */
class m201113_191946_alter_projects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('projects', 'participants', 'managers');
        $this->addColumn('projects', 'workers', $this->json());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('projects', 'managers', 'participants');
        $this->dropColumn('projects', 'workers');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201113_191946_alter_projects_table cannot be reverted.\n";

        return false;
    }
    */
}
