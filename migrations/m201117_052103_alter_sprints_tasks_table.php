<?php

use yii\db\Migration;

/**
 * Class m201117_052103_alter_sprints_tasks_table
 */
class m201117_052103_alter_sprints_tasks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('sprints_tasks', 'plan', $this->integer());
        $this->addColumn('sprints_tasks', 'fact', $this->integer());
        $this->addColumn('sprints_tasks', 'interval', $this->json());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('sprints_tasks', 'plan');
        $this->dropColumn('sprints_tasks', 'fact');
        $this->dropColumn('sprints_tasks', 'interval');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201117_052103_alter_sprints_tasks_table cannot be reverted.\n";

        return false;
    }
    */
}
